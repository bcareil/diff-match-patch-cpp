
set(CUSTOM_TESTS_SOURCES
    "test_diff_match_patch.cpp"
    )


add_executable(test_diff_match_patch_custom ${CUSTOM_TESTS_SOURCES})
target_link_libraries(test_diff_match_patch_custom diff-match-patch)
add_test(NAME test COMMAND test_diff_match_patch_custom)
