Diff Match Patch
================

Copy of `Google's diff-match-patch <https://github.com/google/diff-match-patch>`_ with some minor tweaks to compile with Qt5.

This is an implementation of the `Myer's diff algorithm <https://neil.fraser.name/writing/diff/myers.pdf>`_.

Build
-----

You'll need Qt5 and cmake. Then run::

  $ mkdir build
  $ cd build
  $ cmake ..
  $ cmake --build .

