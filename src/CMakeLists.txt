set(DIFF_MATCH_PATCH_SRC
	"${CMAKE_CURRENT_SOURCE_DIR}/diff_match_patch.cpp"
	"${CMAKE_CURRENT_SOURCE_DIR}/diff_match_patch.h"
	)

add_library(diff-match-patch ${DIFF_MATCH_PATCH_SRC})
target_include_directories(diff-match-patch PUBLIC
	$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
	$<INSTALL_INTERFACE:include/>
	)
target_link_libraries(diff-match-patch PUBLIC Qt5::Core)
